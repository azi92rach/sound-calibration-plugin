# !/usr/bin/python3
# -*- coding: utf-8 -*-

from pyforms import conf

from AnyQt.QtGui import QIcon
from sound_calibration_plugin.sound_windows import SoundWindow


class ProjectsSound(object):
    def __init__(self, mainwindow=None):
        super(ProjectsSound, self).__init__(mainwindow)

    # Open it when the Gui is opened
    # self.open_sound_plugin()

    def register_on_main_menu(self, mainmenu):
        super(ProjectsSound, self).register_on_main_menu(mainmenu)

        if len([m for m in mainmenu if 'Tools' in m.keys()]) == 0:
             mainmenu.append({'Tools': []})

        menu_index = 0
        for i, m in enumerate(mainmenu):
            if 'Tools' in m.keys():
                menu_index = i
                break

        mainmenu[menu_index]['Tools'].append('-')
        mainmenu[menu_index]['Tools'].append(
            {'Sound Calibration': self.open_sound_plugin, 'icon': QIcon(conf.SOUND_PLUGIN_ICON)})

    def open_sound_plugin(self):
        if not hasattr(self, 'sound_plugin'):
            self.sound_plugin = SoundWindow(self)
            self.sound_plugin.show()
            self.sound_plugin.subwindow.resize(*conf.SOUND_PLUGIN_WINDOW_SIZE)
        else:
            self.sound_plugin.show()

        return self.sound_plugin
