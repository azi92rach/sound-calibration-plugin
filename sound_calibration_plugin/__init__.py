# !/usr/bin/python3
# -*- coding: utf-8 -*-

__version__ = "5.0"
__author__ 		= ['Rachid Azizi']
__credits__ 	= ['Rachid Azizi']
__license__ 	= "Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>"
__maintainer__ 	= ['Rachid Azizi']
__email__ 		= ['azi92rach@gmail.com']
__status__ 		= "Development"

from pyforms import conf

conf += 'sound_calibration_plugin.settings'
