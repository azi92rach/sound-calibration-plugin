# !/usr/bin/python3
# -*- coding: utf-8 -*-

import logging
import time

import numpy as np
import PyCmdMessenger
from AnyQt.QtCore import QThread
from scipy import signal as si

import sound_calibration_plugin.logbook as Logbook
import sound_calibration_plugin.settings as setting
import sound_calibration_plugin.sound_utils as sut
from pyforms import conf
from toolsR import SoundR
import datetime

logger = logging.getLogger(__name__)
CRED = '\033[46;1;41m'
CEND = '\033[0m'

BAUD_RATE = 9600 #  By default
PRESSURE_REF = 0.00002 #  20 uPa

class ArduinoTaskThread(QThread):
    """
    This thread performs the actual calibration. It is designed as a state machine
    implementing the following algorithm: it starts calibration (self.STATE = 'init')
    by obtaining the recorded dBs coming from the microphone. It then checks
    whether the calibration is completed or not, and whether our target value
    is higher or lower than the obtained. According to that, it modifies the volume
    in the Arduino shield and starts again. If the record is higher, then lower,
    then higher again, it means that our specified tolerance is too tight.
    """
    def __init__(self, dbTarget=None, tolerance=None, iterations=None,
                sens=None, gainM=None, FsIn=None, chDaq=None, secDaq=None, messenger=None,
                windows=None, box=None, side=None):

        QThread.__init__(self)
        self.dbTarget = dbTarget
        self.tolerance = tolerance
        self.iterations = iterations
        self.sens = sens
        self.gainM = gainM
        self.FsIn = FsIn
        self.chDaq = chDaq
        self.secDaq = secDaq
        self.messenger = messenger
        self.windows = windows
        self.box = box
        self.side = side

        #Init variables:
        self.received_average = 0
        self.guess_record = []
        self.guess = 25
        # Initial state:
        self.STATE = 'init'
        self.MAX = False
    
    def __del__(self):
        self.wait()

    def run(self):
        """
        There are four exit states: 'finished' is when the calibration ended
        successfully; 'min' is when we reached minimum shield volume (30); 'max'
        is when we reached maximum shield value (1); 'too_tight' is when we couldn't
        complete the calibration due to the tolerance being too tight.
        """
        self.windows.mode = 1
        exit_states = {'finished', 'too_tight', 'min', 'max'}
        while self.STATE not in exit_states:
            self.guess_record.append(self.guess)
                        
            logging.warning(f"state: {self.STATE}")

            if self.STATE == 'init':
                self._getdB()
                self.STATE = self._check_cal()
                self._modify_guess()
            elif self.STATE == 'H':
                self._getdB()
                cal_state = self._check_cal()
                if cal_state == 'finished' or cal_state == 'H':
                    self.STATE = cal_state
                elif cal_state == 'L':
                    self.STATE = 'LH'
                self._modify_guess()
            elif self.STATE == 'L':
                self._getdB()
                cal_state = self._check_cal()
                if cal_state == 'finished' or cal_state == 'L':
                    self.STATE = cal_state
                elif cal_state == 'H':
                    self.STATE = 'HL'
                self._modify_guess()
            elif self.STATE == 'LH':
                self._getdB()
                cal_state = self._check_cal()
                if cal_state == 'finished':
                    self.STATE = 'finished'
                elif cal_state == 'H':
                    self.STATE = 'too_tight'
                self._modify_guess()
            elif self.STATE == 'HL':
                self._getdB()
                cal_state = self._check_cal()
                if cal_state == 'finished':
                    self.STATE = 'finished'
                elif cal_state == 'L':
                    self.STATE = 'too_tight'
                self._modify_guess()

            if self.guess == 1 and self.MAX: 
                self.STATE = 'max'
            elif self.guess == 1:
                self.MAX = True
            elif self.guess > 40:
                self.STATE = 'min'
            
            self.windows._listResults.append(self.received_average)
            self.windows._graph.value = self.windows._on_draw

        logging.warning(f"{self.windows._listResults}")
        lenResult = len(self.windows._listResults)
        logging.warning(f"{lenResult}")
        logging.warning("Cal finished")  
        logging.warning(f"{self.STATE}")  
        if self.STATE == 'too_tight':
            self.windows.warning("Tolerance is too tight.", "Warning")
        elif self.STATE == 'max':
            self.windows.warning("MAX value reached.", "Warning")
        elif self.STATE == 'min':
            self.windows.warning("MIN value reached.", "Warning")
        else: # that is, self.STATE == 'finished'
            self._savelog()
            self._saveEEprom()
            if lenResult == 1:
                self.windows.info("The speaker is already calibrated.", "Success")
            else:
                self.windows.info(f"The speaker is calibrated after {len(self.windows._listResults) - 1} iterations.", "Success")
        self.windows.mode = 1  # see sound_windows.py
        self.windows._graph.value = self.windows._on_draw  # Plot the results             
        self.windows._taskisRunning = False

    def _savelog(self):
        if self.side == 0:
            side = "LR"
        elif self.side == 1:
            side = "L"
        else:
            side = "R"
        date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
        data = [self.box, date, side, self.dbTarget, self.tolerance, round(self.received_average, 2), self.guess_record[-1]]
        header = ["Box", "Date", "Side", "Target", "Tolerance", "FinalVal", "Volume"]        
        log = Logbook.Logbook(setting.SOUND_PLUGIN_ARDUINO_CAL, "calibration_logs.csv")
        log.save(data, header)      

    def _saveEEprom(self):
        logging.warning("Saving to EEPROM")
        self.messenger.send("save_data", self.guess_record[-1])

    def _modify_guess(self):
        """
        This method is called just before a state transition. It calculates the 
        next guess, taking into account that it can't go below 1.
        """
        if self.STATE == 'H':
            self.guess += 5
        elif self.STATE == 'L':
            self.guess -= 5
        elif self.STATE == 'HL':
            self.guess += 1
        elif self.STATE == 'LH':
            self.guess -= 1
        # The Arduino doesn't accept values lower than 1:
        if self.guess < 1:
            self.guess = 1          

    def _getdB(self):
        """
        Asks the Arduino for a sound and captures it with the mic + DaQ; it does it
        a number of times specified by self.iterations and computes an average.
        """
        self.received_average = 0
        for _ in range(self.iterations):
            self.messenger.send("send_volume", self.guess, self.side)
            time.sleep(0.1)
            self.messenger.send("play_sound1")
            time.sleep(0.15)
            data, _ = sut.scanSignal(self.chDaq, self.secDaq, self.FsIn, self.sens)
            logging.warning(f"{data}")
            self.received_average += self._computedB(data) / self.iterations 
        logging.warning(f"{CRED}Received dB's: {self.received_average}, {self.guess}{CEND}") 

    def _check_cal(self):
        """
        Returns 'finished' if the conditions upon which the calibration is completed are
        met: the received value is between the target dB band or the loop completes
        15 iterations. Otherwise, returns 'H' if the recorded dB value is higher
        than the target or 'L' if it's the opposite.
        """
        tolerance = np.abs(self.dbTarget - self.received_average) < self.tolerance

        higher = self.dbTarget < self.received_average
        lower = self.dbTarget >= self.received_average

        if tolerance:
            return 'finished'
        elif higher:
            return 'H'
        elif lower:
            return 'L'        

    def _computedB(self, data):
        """
        Computes the sound level in dB of a white noise signal (whole available band).
        """
        # Compute the average of the incoming noise signal and receive it in [Pa]:
        aux = [elem for elem in data if not np.isinf(elem)]
        filtered_signal = ArduinoTaskThread.butter_highpass_filter(aux, 1000, 500000)
        average = np.average(np.sqrt(np.square(filtered_signal)))
        logging.warning(f"{average}")
        # Divide by the amplifier gain:
        pa = average / 100
        logging.warning(f"Pa: {pa}")
        # convert to dBs referred to 20 uPa:
        return 20 * np.log10(pa / PRESSURE_REF)

    @staticmethod
    def butter_highpass(cutoff, fs, order=5):
        """
        This method computes the coefficients of a Butterworth high-pass
        filter.
         - cutoff: cutoff frequency in Hz
         - fs: sampling frequency of the signal (remember that this is a digital filter)
         - order: filter order
        returns:
         - numerator (b) and denominator (a) of the filter expression.
        """
        nyq = 0.5 * fs
        normal_cutoff = cutoff / nyq
        b, a = si.butter(order, normal_cutoff, btype='high', analog=False)
        return b, a
        
    @staticmethod
    def butter_highpass_filter(data, cutoff, fs, order=5):
        """
        This method applies a high-pass filter to incoming data.
        Returns: the filtered signal.
        """
        b, a = ArduinoTaskThread.butter_highpass(cutoff, fs, order=order)
        filtered = si.filtfilt(b, a, data)
        return filtered

class TaskArduinoCalib:
    """
    Arduino calibration procedure. It activates a thread so as not to block
    the main GUI window.
    """        

    def __init__(self, windows=None, dbTarget=None, tolerance=None, iterations=None,
                sens=None, gainM=None, FsIn=None, chDaq=None, secDaq=None, device=None, side=None):

        self.windows = windows        
        self.windows._taskisRunning = True
        self.device = device                
        self.side = side

        try:
            # Init comms with Arduino:
            arduino = PyCmdMessenger.ArduinoBoard(self.device, baud_rate=BAUD_RATE)
            commands = [["play_sound1",""],
                        ["play_sound2",""],
                        ["device_ready",""],
                        ["error","s"],
                        ["send_volume", "ii"],
                        ["cmd_box", "s"],
                        ["save_data", "i"]]
            # Initialize the messenger:
            self.messenger = PyCmdMessenger.CmdMessenger(arduino,commands)
            # Send the side (R, L, R/L):
            self.messenger.send("device_ready")
            _, box, _ = self.messenger.receive()
            box_str = box[0]
        except Exception as err:
            self.windows._taskisRunning = False
            self.windows.critical("I couldn't find the Arduino. Try changing the port. \n "
                                 "E: task_arduino_cal", "Error")
        else:
            logging.warning(f"Box: {box}")
            # Start the thread that contains the task:
            self.get_thread = ArduinoTaskThread(dbTarget=dbTarget, tolerance=tolerance, iterations=iterations,
                    sens=sens, gainM=gainM, FsIn=FsIn, chDaq=chDaq, secDaq=secDaq, messenger=self.messenger,
                    windows=windows, box=box_str, side=self.side)            
            try:
                self.get_thread.start()
            except Exception as err:
                self.windows._taskisRunning = False
                logger.error(str(err), exc_info=True)
                self.windows.critical("Unexpected error while running sound calibration. Pleas see log for more details.\n "
                                    "E: task_arduino_cal", "Error")  

    def done(self):
        logging.warning("A")
